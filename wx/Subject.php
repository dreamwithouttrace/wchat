<?php


namespace wchat\wx;



/**
 * Class Subject
 * @package wchat\wx
 */
class Subject extends \wchat\base\Subject
{


	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return 'cgi-bin/message/subscribe/send';
	}


	/**
	 * @return string
	 */
	public function getHost(): string
	{
		return 'api.weixin.qq.com';
	}
}
