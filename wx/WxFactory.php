<?php

namespace wchat\wx;

use Kiri\Di\Container;
use wchat\common\AppConfig;

class WxFactory
{


    /**
     * @param string $class
     * @param AppConfig $config
     * @return object|null
     * @throws
     */
    public static function get(string $class, AppConfig $config): ?object
    {
        $container = Container::instance();
        $object    = $container->get($class);
        $object->setPayConfig($config);
        return $object;
    }


}
