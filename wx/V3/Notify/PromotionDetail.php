<?php

namespace wchat\wx\V3\Notify;

class PromotionDetail
{

    public int    $amount               = 100;
    public int    $wechatpay_contribute = 0;
    public string $coupon_id            = "109519";
    public string $scope                = "GLOBAL";
    public int    $merchant_contribute  = 0;
    public string $name                 = "单品惠-6";
    public int    $other_contribute     = 0;
    public string $currency             = "CNY";
    public string $stock_id             = "931386";

    /**
     * @var array<GoodsDetail>
     */
    public array $goods_detail = [];

}