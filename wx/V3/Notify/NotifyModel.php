<?php

namespace wchat\wx\V3\Notify;

use JetBrains\PhpStorm\ArrayShape;

class NotifyModel
{

    //公众号支付
    const string PAY_TYPE_JSAPI = 'JSAPI';

    //扫码支付
    const string PAY_TYPE_NATIVE = 'NATIVE';

    //App支付
    const string PAY_TYPE_App = 'App';

    //付款码支付
    const string PAY_TYPE_MICROPAY = 'MICROPAY';

    //H5支付
    const string PAY_TYPE_MWEB = 'MWEB';

    //刷脸支付
    const string PAY_TYPE_FACEPAY = 'FACEPAY';


    // 支付成功
    const string PAY_RESULT_SUCCESS = 'SUCCESS';

    // 转入退款
    const string PAY_RESULT_REFUND = 'REFUND';

    // 未支付
    const string PAY_RESULT_NOTPAY = 'NOTPAY';

    // 已关闭
    const string PAY_RESULT_CLOSED = 'CLOSED';

    // 已撤销（付款码支付）
    const string PAY_RESULT_REVOKED = 'REVOKED';

    // 用户支付中（付款码支付）
    const string PAY_RESULT_USERPAYING = 'USERPAYING';

    // 支付失败(其他原因，如银行返回失败)
    const string PAY_RESULT_PAYERROR = 'PAYERROR';


    public string $appid;
    public string $mchid;
    public string $out_trade_no;
    public string $transaction_id;
    public string $trade_type;
    public string $trade_state;
    public string $trade_state_desc;
    public string $bank_type;
    public string $attach;
    public string $success_time;

    /**
     * @var array|string[]
     */
    #[ArrayShape(['openid' => 'string'])]
    public array $payer = ['openid' => ''];


    /**
     * @var array
     */
    #[ArrayShape(['payer_total' => 'int', 'total' => 'int', 'currency' => 'string', 'payer_currency' => 'string'])]
    public array $amount = [
        "payer_total"    => 100,
        "total"          => 100,
        "currency"       => "CNY",
        "payer_currency" => "CNY"
    ];


    /**
     * @var array|string[]
     */
    #[ArrayShape(['device_id' => 'string'])]
    public array $scene_info = [
        'device_id' => ''
    ];

    /**
     * @var array<PromotionDetail>
     */
    public array $promotion_detail = [];
}