<?php


namespace wchat\wx;


use wchat\common\Result;

class Token extends SmallProgram
{

    /**
     * @return Result
     */
    public function token(): Result
    {
        $query = ['grant_type' => 'client_credential'];
        if ($this->payConfig->typeIsApp()) {
            $query['appid']  = $this->payConfig->pay->wx->appId;
            $query['secret'] = $this->payConfig->pay->wx->appSecret;
        } else {
            $query['appid']  = $this->payConfig->appId;
            $query['secret'] = $this->payConfig->appSecret;
        }
        return $this->get('api.weixin.qq.com', 'cgi-bin/token', $query);
    }


}
