<?php

namespace wchat\common\libs;

class Aliyun
{

    // APPID
    public string $appId;

    // APP公钥
    public string $appKey;

    // APP私钥
    public string $appSecret;

    // APP公钥证书
    public string $appPubSecret;

    // 阿里云公钥证书
    public string $aliPubSecret;

    // 阿里云根证书
    public string $aliRootSecret;

    // 是否开启证书模式
    public int $openFileState;

}