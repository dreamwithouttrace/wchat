<?php

namespace wchat\common\libs;

class Qq
{


    /**
     * @var string
     */
    public string $appId;


    /**
     * @var string
     */
    public string $mchCa;


    /**
     * @var string
     */
    public string $mchId;


    /**
     * @var string
     */
    public string $mchKey;


    /**
     * @var string
     */
    public string $mchCert;


    /**
     * @var string
     */
    public string $appSecret;


    /**
     * @var string
     */
    public string $mchSecret;

}