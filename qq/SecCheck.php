<?php


namespace wchat\qq;


use wchat\common\Result;

/**
 * Class SecCheck
 * @package qq
 */
class SecCheck extends SmallProgram
{

    private string $_url = '/api/json/security/ImgSecCheck?access_token=';

    private string $_msgUrl = '/api/json/security/MsgSecCheck?access_token=';

    /**
     * @param string $path
     * @return Result
     */
    public function image(string $path = ''): Result
    {
        if (!file_exists($path)) {
            return $this->sendError('文件不存在', 404);
        }

        $real_path = new \CURLFile($path);
        $data      = [
            'appid'                   => $this->payConfig->appId,
            'media'                   => $real_path,
            'form-data[filename]'     => $path,
            'form-data[content-type]' => $real_path->getMimeType()
        ];
        return $this->upload('api.q.qq.com', $this->_url, $data);
    }


    /**
     * @param string $content
     * @return Result
     */
    public function text(string $content): Result
    {
        if (empty($content)) {
            return $this->sendError('文件不存在', 404);
        }
        $url = '/' . ltrim($this->_msgUrl, '/') . $this->payConfig->getAccessToken();

        return $this->post('api.q.qq.com', $url, ['appid' => $this->payConfig->appId, 'content' => $content]);
    }

}
