<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/19 0019
 * Time: 16:12
 */

namespace wchat\qq;

use wchat\common\Result;

class Account extends SmallProgram
{

    /**
     * @param string $code
     * @return Result
     */
    public function login(string $code): Result
    {
        $param['appid']      = $this->payConfig->appId;
        $param['secret']     = $this->payConfig->appSecret;
        $param['js_code']    = $code;
        $param['grant_type'] = 'authorization_code';

        return $this->get('api.q.qq.com', '/sns/jscode2session', $param);
    }


    /**
     * @param string $code
     * @return Result
     */
    public function appLogin(string $code): Result
    {
        $param['appid']      = $this->payConfig->appId;
        $param['secret']     = $this->payConfig->appSecret;
        $param['js_code']    = $code;
        $param['grant_type'] = 'authorization_code';

        $this->host = 'graph.qq.com';

        return $this->get('api.q.qq.com', '/user/get_user_info', $param);
    }

}
