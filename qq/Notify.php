<?php


namespace wchat\qq;

use Exception;
use wchat\common\Help;

/**
 * Class Notify
 * @package wchat\qq
 */
class Notify extends SmallProgram
{
    public mixed $appid          = null;
    public mixed $mch_id         = null;
    public mixed $nonce_str      = null;
    public mixed $sign           = null;
    public mixed $device_info    = null;
    public mixed $trade_type     = null;
    public mixed $trade_state    = null;
    public mixed $bank_type      = null;
    public mixed $fee_type       = null;
    public mixed $total_fee      = null;
    public mixed $cash_fee       = null;
    public mixed $coupon_fee     = null;
    public mixed $transaction_id = null;
    public mixed $out_trade_no   = null;
    public mixed $attach         = null;
    public mixed $time_end       = null;
    public mixed $openid         = null;

    /**
     * @return bool
     * 判断是否完成支付
     */
    public function isSuccess(): bool
    {
        return $this->trade_state === 'SUCCESS';
    }

    /**
     * @param array $params
     * @return $this
     * @throws
     */
    public function setPayNotifyData(array $params): static
    {
        if (!$this->validation($params)) {
            throw new Exception('签名错误！');
        }
        foreach ($params as $key => $val) {
            $this->$key = $val;
        }
        return $this;
    }


    public function __set(string $name, $value): void
    {
        if (property_exists($this, $name)) {
            $this->{$name} = $value;
        }
    }


    /**
     * @param array $params
     * @return bool
     */
    public function validation(array $params): bool
    {
        $sign = $params['sign'];
        unset($params['sign']);

        $signType   = $this->payConfig->getSignType();
        $privateKey = $this->payConfig->pay->qq->mchSecret;
        $nowSign    = Help::sign($params, $privateKey, $signType);
        if ($sign === $nowSign) {
            return true;
        }
        return false;
    }

    /**
     * @return null
     */
    public function getAppid(): mixed
    {
        return $this->appid;
    }


}
