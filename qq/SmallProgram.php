<?php


namespace wchat\qq;


use Kiri\Client;
use wchat\common\Multiprogramming;

class SmallProgram extends Multiprogramming
{


    /**
     * @var string
     */
    protected string $host = 'api.q.qq.com';


    /**
     * @param string $url
     * @param mixed $data
     * @return Client
     */
    protected function createClient(string $url, mixed $data): Client
    {
        $client = new Client('api.qpay.qq.com', 443, true);
        $client->withHeader(['Content-Type' => 'application/json']);
        $client->withSslCertFile($this->payConfig->pay->qq->mchCert);
        $client->withSslKeyFile($this->payConfig->pay->qq->mchKey);
        $client->withCa($this->payConfig->pay->qq->mchCa);
        $proxyHost = $this->payConfig->getProxyHost();
        $proxyPort = $this->payConfig->getProxyPort();
        if (!empty($proxyHost) && $proxyPort > 0) {
            $client->withProxyHost($proxyHost)->withProxyPort($proxyPort);
        }
        $client->post($url, $data);
        $client->close();
        return $client;
    }

}
